﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeautySalon.DB
{
    using System;
    using System.Collections.Generic;

    public partial class Product
    {
        public int Quantity { get; set; } = 1;
    }
}
