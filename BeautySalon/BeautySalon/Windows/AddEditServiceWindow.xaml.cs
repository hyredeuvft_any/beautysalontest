﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BeautySalon.DB;
using BeautySalon.Windows;

namespace BeautySalon.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddEditServiceWindow.xaml
    /// </summary>
    public partial class AddEditServiceWindow : Window
    {
        public AddEditServiceWindow()
        {
            InitializeComponent();
        }

        public AddEditServiceWindow(Service service)
        {
            InitializeComponent();
        }

        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            ListServiceWindow listServiceWindow = new ListServiceWindow();
            listServiceWindow.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            ListServiceWindow listServiceWindow = new ListServiceWindow();
            listServiceWindow.Show();
            this.Close();
        }
    }
}
