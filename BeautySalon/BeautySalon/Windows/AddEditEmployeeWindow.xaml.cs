﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BeautySalon.DB;
using BeautySalon.Windows;

namespace BeautySalon.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddEditEmployeeWindow.xaml
    /// </summary>
    public partial class AddEditEmployeeWindow : Window
    {
        public AddEditEmployeeWindow()
        {
            InitializeComponent();
        }

        public AddEditEmployeeWindow(Employee employee)
        {
            InitializeComponent();
        }

        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            ListEmployeeWindow listEmployeeWindow = new ListEmployeeWindow();
            listEmployeeWindow.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            ListEmployeeWindow listEmployeeWindow = new ListEmployeeWindow();
            listEmployeeWindow.Show();
            this.Close();
        }
    }
}
